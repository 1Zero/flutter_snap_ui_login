import 'package:flutter/material.dart';

const estilodetexto = TextStyle(
  color:Colors.white54,
  fontFamily: 'OpenSans',
);

const etiquetaestilo = TextStyle(

  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontFamily: 'OpenSans',
);

final estilodecaja = BoxDecoration(
  color: const Color(0xFF6CA8F1),
  borderRadius: BorderRadius.circular(10.0),
  boxShadow:const[
    BoxShadow(
      color:Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0,2),
    ),
  ]
);
