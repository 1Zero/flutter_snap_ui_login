import 'package:flutter/material.dart';
import 'package:ui_login/features/login/presentation/widgets/widgets.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({ Key? key }) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Stack(children:  <Widget>[

        Container(height:double.infinity ,
          width: double.infinity,
          decoration:const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
              Color(0xFF73AEF5),
              Color(0xFF61A4F1),
              Color(0xFF478DE0),
              Color(0xFF398AE5),
              ],
              stops: [
              0.1,
              0.4,
              0.7,
              0.9
            ]
            )
          ),
        ),
        // ignore: sized_box_for_whitespace
        Container(
          
          //color: Colors.amber,
         height: double.infinity,
         width: double.infinity,
          child:  SingleChildScrollView(
            
              physics: const AlwaysScrollableScrollPhysics(),
              padding: const EdgeInsets.symmetric(
                horizontal: 40.0,
                vertical: 60.0,
                
                
              ),
              child: Column(
                
                mainAxisAlignment:MainAxisAlignment.center ,
                children:  <Widget>[
                  const Text('iniciar sesion',
                    style:TextStyle(
                      color: Colors.white,
                      fontFamily: 'OpenSans',
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold
                    ),                  
                  ),
                  const SizedBox(
                    height: 30.0,
                  ),

                  BuildUser(),
                  const SizedBox(                    
                    height: 30.0,
                  ),                 
                  BuildPassword(),
                  const BuildForgotPassword(),
                  const BuildCheckBox(),
                  const BuildLogin(),
                  BuildTexto(),
                  const BuildRedesSociales(),
                  
                 
                 
                 
                ],
              ),
          ),
        )
      ]
      
      )
      
    );
  }
}