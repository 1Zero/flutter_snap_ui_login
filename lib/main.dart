import 'package:flutter/material.dart';
import 'package:ui_login/screen/login_screen.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'login UI',
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
        
      
    );
  }
}